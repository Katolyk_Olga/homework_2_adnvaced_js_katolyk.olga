
// Рішення з використанням try catch

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const divRoot = document.createElement("div");  // Створюємо div з id="root", додаємо на сторінку
divRoot.id = "root";
document.body.append(divRoot);

const ulBooks = document.createElement("ul");  // Створюємо тег ul, додаємо контекст і стилі
ulBooks.innerText = "Our library:"
ulBooks.style.fontWeight = "bold";
ulBooks.style.fontSize = "24px";

try {    // Пробуємо спіймати помилку
    for (const book of books) {  // Перебираємо масив books
        const li = document.createElement("li");
        if (!book.author || !book.name || !book.price) {
            // throw new Error // викидання помилки не підходить тому, що миттєво зупиняє код і далі не йде
            console.error
                (`This object is missing the required property - 
                    ${!book.author ? 'author' : !book.name ? 'name' : 'price'}:
                    ${JSON.stringify(book)}`);  // Створюємо конекст помилки з указанням проблеми
        } else {
            li.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}\;`;
            li.style.fontWeight = "normal";
            li.style.fontSize = "18px";
            ulBooks.appendChild(li);
        }
    }
} catch (error) {
    console.error(error.message);
}
divRoot.append(ulBooks);  // Додаємо ul до div з id="root"



// for (const book of books) {  // Перебираємо масив books
//    try {
//             // Спроба виконати код, який може викликати помилку
//             const li = document.createElement("li");
//             if (!book.author || !book.name || !book.price) {

//                 // Додавання властивостей, які відсутні
//                 book.author = book.author || 'Unknown';
//                 book.name = book.name || 'Unknown';
//                 book.price = book.price || 'Unknown';

//                 // Задаємо стиль display: none, щоб приховати li
//                 li.style.display = 'none';

//                 throw new Error(`Invalid object - missing properties: ${JSON.stringify(book)}`);
//             }

//             li.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
//             li.style.fontWeight = "normal";
//             li.style.fontSize = "18px";

//             ulBooks.appendChild(li);
//         } catch (error) {
//             // Обробка помилки та виведення в консоль
//             console.error(error.message);
//         }
//     }